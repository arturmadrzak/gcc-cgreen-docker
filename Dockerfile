FROM  gcc
LABEL description="Docker image for build and run cgreen-devs based unit tests"
LABEL maintainer="Artur Mądrzak <artur@madrzak.eu>"

RUN apt-get update --yes && \
    apt-get install --yes cmake git
    
RUN git clone --depth=1 https://github.com/cgreen-devs/cgreen && \
    cd cgreen && \
    mkdir build && cd build && \
    cmake .. && \
    make && make install && \
    ldconfig /usr/lib && \
    cd .. && rm -rf cgreen
    
RUN apt-get remove --yes cmake
