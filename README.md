# gcc-cgreen-docker

Docker image based on gcc with latest cgreen installed.

Image is deployed by default to gitlab.com container registry and can be used
in other repositories. 

.gitlab-ci.yml:
```
...
test:
  stage: test
  image: registry.gitlab.com/arturmadrzak/gcc-cgreen-docker
  script:
    - make test
...

```